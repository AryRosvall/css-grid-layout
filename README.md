##Conceptos Básicos



**Grid Container:** Elemento padre.

**Grid Item**: Componentes hijos de un Grid Container.

**Grid Line**: Líneas invisibles divisorias horizontales y verticales. 

**Grid Track**: Filas y columnas.

**Grid Cell**: Celdas.

**Grid Area**: Espacio rodeado por 4 grid lines.

##Propiedades

Convertir el display en un grid.

###display:grid

Generar un grid. 

####_Sintaxis_

```css
display:grid 
```

###grid-template-columns

Definir el número de columnas que existirán en el grid.

####_Sintaxis_

```css
grid-template-columns: 200px 200px 200px; /*Sintaxis (tamaño_col tamaño_col2 tamaño_colN)*/
```

###grid-template-rows

Definir el número de filas que existirán en el grid.

####_Sintaxis_

```css
grid-template-rows: 200px 200px 200px; /*Sintaxis (tamaño_row tamaño_row2 tamaño_rowN)*/
```

###grid-template

Definir en una sola propiedad filas y columnas.

####_Sintaxis_

```css
grid-template: 200px 200px 200px / 100px 20% 30% 200px; /*Sintaxis (filas / columnas)*/
```

```css
grid-template: repeat(3, 100px) / repeat(3, 100px); /*Sintaxis ( repeat(# de filas, tamaño) / repeat(# de columnas, tamaño))*/
```

###grid-gap

Permite agregar espacio entre filas y columnas

####_Sintaxis_

```css
grid-column-gap: 200px; /* Espaciado entre columnas*/
```

```css
grid-row-gap: 200px; /* Espaciado entre filas*/
```

```css
grid-gap: 20px 200px; /*Sintaxis (filas / columnas)*/
```

###grid-template-areas

Permite definir áreas de contenido.

####_Sintaxis_

En el elemento padre se debe usar la propiedad: 

```css
grid-template-areas: "header header" 
                     "left contenido" 
                     "footer footer";
```

Donde: 

*Van a existir 2 columnas y tres filas. 
*El header ocupa el espacio de las dos columnas al igual que el footer.
*Existe un elemento left y uno llamado contenido que ocuparán una columna cada uno.

En los elementos hijos agregar una clase que contendrá la propiedad:

```css
grid-area: "header"; 
```

Donde: 

"header" es el nombre definido en el elemento padre y tomará el espacio definido en el elemento padre.

###grid-column

Permite definir el tamaño de las columnas.

####_Sintaxis_

Sintaxis sin resumir:

```css
grid-column-start: 1;
grid-column-end: 3;
```

Sintaxis resumida:

```css
grid-column: 1 / 3; /* grid-column: inicio / final; */
```

Donde:

```css
grid-column: 1 / 3; /* Para definir cuántas líneas quieres abarcar */
```

```css
grid-column: 1 / span 2; /* Para definir cuántas columnas quieres abarcar */
```

```css
grid-column: 1 / -1; /* Para ocupar todo el ancho dinámicamente */
```

###grid-row

Permite definir el tamaño de las filas.

####_Sintaxis_

Sintaxis sin resumir:

```css
grid-row-start: 1;
grid-row-end: 3;
```

Sintaxis resumida:

```css
grid-row: 1 / 3; /* grid-row: inicio / final; */
```

Donde:

```css
grid-row: 1 / 3; /* Para definir cuántas líneas quieres abarcar */
```

```css
grid-row: 1 / span 2; /* Para definir cuántas filas quieres abarcar */
```

```css
grid-row: 1 / -1; /* Para ocupar todo el ancho dinámicamente */
```

###Nombrando líneas

Permite definir un nombre para las líneas que dividen las filas y las columnas. 

Notas: 

* Aplica para grid-template-rows/columns.
* No se puede ocupar repeat para definir las líneas.

####_Sintaxis_

```css
 grid-template-rows:[inicio] 200px [línea] 200px [final]; /* grid-template-rows/columns:[nombre_línea1] tamaño fila/columna 1 [nombre_línea2] tamaño fila/columna 2 [nombre_línea3];  */
```

###Manejo de Grid Implícito

Permite definir tamaño de las filas y/o columnas que no tienen un valor asignado. 

Notas: 

* grid-auto-flow por defecto viene configurado como row. Es decir, grid-auto-flow: row;

####_Sintaxis_

```css
 grid-auto-flow: row | column ; 
```

```css
 grid-auto-columns: tamaño de las columnas; 
```

```css
 grid-auto-rows: tamaño de las filas; 
```

* Si se desconoce la cantidad de filas y/o columnas definir un solo tamaño. También si se utiliza Firefox.

###Alineando elementos dentro del grid

Para alinear todos los elementos del grid, en el container se pueden agregar las siguientes propiedades: 

####_Sintaxis_

####justify-items (Horizontal)

```css
justify-items: value;
```

####align-items (Vertical)

```css
align-items: value;
```

###Alineando un solo elemento

Para alinear un solo elemento, en la clase del item, agregar las siguientes propiedades:

####_Sintaxis_

```css
justify-self (HORIZONTAL)
align-self (VERTICAL)
```

Nota: Los valores disponibles para estas propiedades son:

* _start_: contenido hacia la izquierda
* _end_: contenido hacia la derecha
* _center_: contenido al medio
* _stretch_: estira el contenido al espacio que nos de el grid

###Alineando el grid completo

####_Sintaxis_

```css 
/*Horizontal*/
justify-content: start | end | center | stretch | space-around | space-between | space-evenly
```

```css 
/*Vertical*/
align-content: start | end | center | stretch | space-around | space-between | space-evenly
```

##Medidas

**fr** - Fracciones - Divide los elementos en fracciones del tamaño disponible.

**autor** - Distribuye el tamaño de acuerdo al espacio que ocupa su contenido.

##Funciones

**repeat**

Permite repetir elementos.

####_Sintaxis_

```css
repeat(3, 1fr); /* repeat(número de elementos, tamaño de los elementos)*/
```

**minmax**

Permite definir el tamaño mínimo y máximo de un elemento.

####_Sintaxis_

```css
minmax(10px, 1fr); /* minmax(tamaño mínimo, tamaño máximo)*/
```

##Tricks Emmet

Para generar de forma automática múltiples objetos utilizar la siguiente nomenclatura.

```css
.item{contenido#$}*12
```

Donde: 

*   .item = nombre de la clase
*   {} = contenido que irá dentro del div
*   $ = número consecutivo 
*   *12 = se generarán 12 objetos

##Tricks CSS 

Selecciona el N elemento de la clase .item

```css
.item:nth-of-type(N)
```

##Links de Interés 

[A Complete Guide to Grid](https://css-tricks.com/snippets/css/complete-guide-grid/)

[Explicit vs Implicit Grid](https://www.quackit.com/css/grid/tutorial/explicit_vs_implicit_grid.cfm)

[Emmet Documentation](https://docs.emmet.io/)

[Emmet in vsCode](https://code.visualstudio.com/docs/editor/emmet)
